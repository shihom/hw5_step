#!/usr/bin/env python
# -*- coding: utf-8 -*-
#http://step-hw-5.appspot.com/
import webapp2


class MainPage(webapp2.RequestHandler):
	html = '''
	<!doctype html>
	<html>
		<head>
			<meta charset = "utf-8"/>
			<title>Patatokukashi--</title>
		</head>
		<body>
			<h1>Word Shuffler</h1>
			<h3>Enter two words</h3>
				<form>
					<input type=text name=input1><br>
					<input type=text name=input2><br>
					<h3>Press "Shuffle!" to shuffle the words.</h3>
					<button type=submit>Shuffle!</button>
				</form>
		</body>
	</html>
	'''

	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		input1 = self.request.get('input1')
		input2 = self.request.get('input2')
		self.response.write(self.html + '<h1>' + 'Shuffled Word: ' + shuffle(input1, input2) + '</h2>')

def shuffle(input1, input2):
	shuffled = ""
	shorter = input1 if len(input1) < len(input2) else input2
	longer = input1 if input1 != shorter else input2

	for i in range(len(shorter)):
		shuffled += input1[i] + input2[i]

	shuffled += longer[len(shorter):]

	return shuffled


app = webapp2.WSGIApplication([
	('/', MainPage),
], debug=True)
